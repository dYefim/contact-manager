import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import { v4 as uuidv4 } from 'uuid';

export interface Contact {
  id: string;
  firstName: string;
  lastName: string;
  isActive: boolean;
  details?: string;
}

const initialState: Contact[] = [
  {
    id: 'JD',
    firstName: 'John',
    lastName: 'Doe',
    isActive: true,
    details: 'Unusual suspect',
  },
];

export const contactsSlice = createSlice({
  name: 'contacts',
  initialState,
  reducers: {
    add: (state, action: PayloadAction<Omit<Contact, 'id'>>) => {
      const { firstName, lastName, isActive, details } = action.payload;

      const id = uuidv4();

      state.push({
        id,
        firstName,
        lastName,
        isActive,
        details,
      });
    },
    update: (state, action: PayloadAction<Contact>) => {
      const index = state.findIndex((c) => c.id === action.payload.id);

      state[index] = action.payload;
    },
    remove: (state, action: PayloadAction<string>) => {
      const index = state.findIndex((c) => c.id === action.payload);

      state.splice(index, 1);
    },
  },
});

export const { add, remove, update } = contactsSlice.actions;

export default contactsSlice.reducer;
