import { useQuery } from 'react-query';

const BASE_URL = 'https://disease.sh/v3/covid-19';

export const fetchCountryData = async () => {
  const response = await fetch(`${BASE_URL}/countries`);

  if (!response.ok) {
    throw new Error('Failed to fetch country data.');
  }

  return response.json();
};

export const fetchChartsData = async () => {
  const response = await fetch(`${BASE_URL}/historical/all?lastdays=all`);

  if (!response.ok) {
    throw new Error('Failed to fetch charts data.');
  }

  return response.json();
};

export const useCountryData = () => {
  return useQuery('countryData', fetchCountryData);
};

export const useChartsData = () => {
  return useQuery('chartsData', fetchChartsData);
};
