import clsx from 'clsx';

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {}

function Button(props: Props) {
  const genericButtonClasses = 'rounded-lg p-4 m-2 text-white font-semibold';

  const defaultColorClass = 'bg-gray-400 hover:bg-gray-500';

  return (
    <button
      {...props}
      className={clsx(
        genericButtonClasses,
        props.className,
        !props.className?.includes('bg-') && defaultColorClass
      )}
    />
  );
}

export default Button;
