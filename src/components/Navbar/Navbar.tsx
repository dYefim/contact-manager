import NavItem from './NavItem';

function Navbar() {
  return (
    <nav className="flex flex-row md:flex-col justify-center md:justify-start gap-8 p-4 text-xl text-center bg-slate-200">
      <NavItem to="/contacts">Contact</NavItem>
      <NavItem to="/dashboard">Charts and maps</NavItem>
    </nav>
  );
}

export default Navbar;
