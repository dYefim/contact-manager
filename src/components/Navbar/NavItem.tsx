import clsx from 'clsx';
import { Link, LinkProps } from 'react-router-dom';

type Props = LinkProps & React.RefAttributes<HTMLAnchorElement>;

function NavItem(props: Props) {
  return (
    <Link
      {...props}
      className={clsx(
        'text-blue-900 hover:text-blue-600 hover:underline font-bold',
        props.className
      )}
    />
  );
}

export default NavItem;
