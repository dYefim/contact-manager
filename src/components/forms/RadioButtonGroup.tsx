interface Option {
  name: string;
  id: string;
  label: string;
}

interface Props {
  label: string;
  options: Option[];
  indexOfSelected: number;
  select: (o: number) => void;
}

function RadioButtonGroup({ label, options, indexOfSelected, select }: Props) {
  return (
    <div className="grid grid-cols-[min-content_1fr] gap-4 mb-4 items-baseline">
      <label className="w-20 block text-gray-700 text-sm font-bold mb-2">
        {label}
      </label>
      <div>
        {options.map((option, index) => (
          <div className="flex items-center" key={option.id}>
            <input
              type="radio"
              className="form-radio"
              name={option.name}
              id={option.id}
              checked={index === indexOfSelected}
              onChange={() => select(index)}
            />
            <label htmlFor={option.id} className="ml-2">
              {option.label}
            </label>
          </div>
        ))}
      </div>
    </div>
  );
}

export default RadioButtonGroup;
