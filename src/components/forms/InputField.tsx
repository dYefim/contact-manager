interface Props extends React.InputHTMLAttributes<HTMLInputElement> {
  label: string;
  type?: string;
}

function InputField({ label, type = 'text', ...props }: Props) {
  return (
    <div className="grid grid-cols-[min-content_1fr] gap-4 mb-4 items-baseline">
      <label
        className="w-20 block text-gray-700 text-sm font-bold mb-2"
        htmlFor={props.id}
      >
        {label}
      </label>
      <input
        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        {...props}
      />
    </div>
  );
}

export default InputField;
