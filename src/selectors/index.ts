import { RootState } from '../store';

export const contactsSelector = (state: RootState) => state.contacts;

export const contactByIdSelector = (id: string | null) => (state: RootState) =>
  state.contacts.find((contact) => contact.id === id);
