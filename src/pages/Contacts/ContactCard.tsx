import { useState } from 'react';
import { Contact } from '../../features/contactsSlice';

function ContactCard({ firstName, lastName, isActive, details }: Contact) {
  const [showDetails, setShowDetails] = useState(false);

  const toggleDetails = () => {
    setShowDetails(!showDetails);
  };

  return (
    <div className="rounded overflow-hidden shadow-lg bg-white border">
      <div className="px-6 py-4">
        <div className="font-bold text-xl mb-2">
          {firstName} {lastName}
        </div>
        <p className="text-gray-700">{isActive ? 'Active' : 'Inactive'}</p>
      </div>
      {details && (
        <div className="px-6 py-4">
          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            onClick={toggleDetails}
          >
            {showDetails ? 'Hide Details' : 'Show Details'}
          </button>
          {showDetails && (
            <div className="mt-4">
              <p className="text-gray-700">{details}</p>
            </div>
          )}
        </div>
      )}
    </div>
  );
}

export default ContactCard;
