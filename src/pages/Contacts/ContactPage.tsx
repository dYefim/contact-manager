import { useNavigate } from 'react-router-dom';
import { useAppSelector } from '../../store/hooks';
import Button from '../../components/Button';
import NoContactsMessage from './NoContactsMessage';
import ContactsList from './ContactsList';
import { contactsSelector } from '../../selectors';

function ContactPage() {
  const navigate = useNavigate();
  const contacts = useAppSelector(contactsSelector);

  return (
    <main className="p-4">
      <div className="flex flex-col items-center">
        <Button
          className={`bg-amber-500 hover:bg-amber-400`}
          onClick={() => {
            navigate('/contacts/create');
          }}
        >
          Create Contact
        </Button>

        {!contacts.length && <NoContactsMessage />}
      </div>
      <ContactsList />
    </main>
  );
}

export default ContactPage;
