import { useNavigate } from 'react-router-dom';
import InputField from '../../components/forms/InputField';
import RadioButtonGroup from '../../components/forms/RadioButtonGroup';
import { Contact } from '../../features/contactsSlice';

type ContactDetails = Contact;

interface Props {
  contactData: ContactDetails;
  setContactData: React.Dispatch<React.SetStateAction<ContactDetails>>;
}

function ContactForm({ contactData, setContactData }: Props) {
  const navigate = useNavigate();

  const handleChange = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
    setContactData((cd) => {
      return { ...cd, [target.id]: target.value };
    });
  };

  const handleStatusChange = (optionIndex: number) => {
    setContactData((cd) => ({
      ...cd,
      isActive: optionIndex === 0 ? true : false,
    }));
  };

  if (!contactData) {
    navigate('/contacts');
  }

  return (
    <div className="max-w-md mx-auto py-4 px-8 bg-white shadow-lg rounded-lg border">
      <InputField
        value={contactData?.firstName || ''}
        onChange={handleChange}
        label="First Name:"
        id="firstName"
        type="text"
      />

      <InputField
        value={contactData?.lastName || ''}
        onChange={handleChange}
        label="Last Name:"
        id="lastName"
        type="text"
      />

      <RadioButtonGroup
        label="Status:"
        indexOfSelected={contactData?.isActive ? 0 : 1}
        select={handleStatusChange}
        options={[
          { id: 'active', name: 'radioGroup', label: 'Active' },
          { id: 'inactive', name: 'radioGroup', label: 'Inactive' },
        ]}
      />
    </div>
  );
}

export default ContactForm;
