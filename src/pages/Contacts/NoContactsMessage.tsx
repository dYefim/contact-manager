import { ReactComponent as CloseIcon } from '../../assets/close-icon.svg';

function NoContactsMessage() {
  return (
    <div className="flex flex-row max-w-md bg-red-100 border border-red-400 text-red-700 p-6 rounded">
      <span className="pr-4">
        <CloseIcon />
      </span>
      <div>
        <strong className="font-bold">No Contact found</strong>
        <span className="block">
          No Contact found. Please add contact from Create Contact button
        </span>
      </div>
    </div>
  );
}

export default NoContactsMessage;
