import { useAppDispatch, useAppSelector } from '../../store/hooks';
import Button from '../../components/Button';
import ContactCard from './ContactCard';
import { remove } from '../../features/contactsSlice';
import { useNavigate } from 'react-router-dom';
import { contactsSelector } from '../../selectors';

function ContactsList() {
  const dispatch = useAppDispatch();
  const contacts = useAppSelector(contactsSelector);
  const navigate = useNavigate();

  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 gap-4">
      {contacts.map((contact) => {
        const handleDelete = () => {
          dispatch(remove(contact.id));
        };

        const openEditContactPage = () => {
          navigate(`/contacts/edit?contactId=${contact.id}`);
        };

        return (
          <div key={contact.id}>
            <ContactCard {...contact} />
            <div className="flex justify-evenly">
              <Button
                className="bg-green-400 hover:bg-green-500"
                onClick={openEditContactPage}
              >
                Edit
              </Button>
              <Button
                className="bg-red-400 hover:bg-red-500"
                onClick={handleDelete}
              >
                Delete
              </Button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default ContactsList;
