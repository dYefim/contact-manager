import { useState } from 'react';
import { useAppDispatch } from '../../store/hooks';
import { add } from '../../features/contactsSlice';
import Button from '../../components/Button';
import ContactForm from '../../pages/Contacts/ContactForm';
import { Contact } from '../../features/contactsSlice';
import { useNavigate } from 'react-router-dom';

const emptyContactData = {
  id: '',
  firstName: '',
  lastName: '',
  isActive: true,
  details: '',
};

function CreateContactPage() {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const [contactData, setContactData] =
    useState<Contact>(emptyContactData);

  const createContact = () => {
    dispatch(add(contactData));
    setContactData(emptyContactData);
    navigate('/contacts');
  };

  return (
    <main>
      <div className="grid justify-center ">
        <h1 className="text-2xl">Create Contact</h1>
        <ContactForm
          contactData={contactData}
          setContactData={setContactData}
        />
        <Button onClick={createContact}>Save contact</Button>
      </div>
    </main>
  );
}

export default CreateContactPage;
