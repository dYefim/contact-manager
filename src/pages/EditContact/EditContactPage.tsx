import { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { update } from '../../features/contactsSlice';
import Button from '../../components/Button';
import ContactForm from '../../pages/Contacts/ContactForm';
import { Contact } from '../../features/contactsSlice';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { contactByIdSelector } from '../../selectors';

function EditContactPage() {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const [searchParams] = useSearchParams();

  const contactId = searchParams.get('contactId');

  const currentContactData = useAppSelector(contactByIdSelector(contactId));

  const [contactData, setContactData] = useState<Contact>(currentContactData!);

  useEffect(() => {
    if (!currentContactData || !contactData) {
      navigate('/contacts');
    }
  }, [contactData, currentContactData, navigate]);

  const updateContact = () => {
    if (!contactData) return;

    dispatch(update(contactData));
    navigate('/contacts');
  };

  return (
    <main>
      <div className="grid justify-center ">
        <h1 className="text-2xl">Create Contact</h1>
        <ContactForm
          contactData={contactData}
          setContactData={setContactData}
        />
        <Button onClick={updateContact}>Save contact</Button>
      </div>
    </main>
  );
}

export default EditContactPage;
