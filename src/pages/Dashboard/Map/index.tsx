import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import markerIconPng from 'leaflet/dist/images/marker-icon.png';
import { Icon } from 'leaflet';
import 'leaflet/dist/leaflet.css';

import { useCountryData } from '../../../api/api';
import { abbreviateNumber } from '../../../utils/abbrevianeNumUtil';

const iconConfig = new Icon({
  iconUrl: markerIconPng,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
});

interface CountryInfo {
  lat: number;
  long: number;
}

interface CountryData {
  country: string;
  active: number;
  cases: number;
  deaths: number;
  countryInfo: CountryInfo;
}

const Map = () => {
  const { data, isLoading } = useCountryData();

  if (isLoading) {
    return <h1>Loading Map... </h1>;
  }

  return (
    <MapContainer
      center={[40.73061, -73.935242]}
      zoom={4}
      style={{ height: '50vh', width: '100%' }}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {data.map((country: CountryData) => (
        <Marker
          key={country.country}
          position={[country.countryInfo.lat, country.countryInfo.long]}
          icon={iconConfig}
        >
          <Popup>
            <h1 className="font-bold">{country.country}</h1>
            <p>Total number of active: {abbreviateNumber(country.active)}</p>
            <p>Recovered cases: {abbreviateNumber(country.cases)}</p>
            <p>Deaths: {abbreviateNumber(country.deaths)}</p>
          </Popup>
        </Marker>
      ))}
    </MapContainer>
  );
};

export default Map;
