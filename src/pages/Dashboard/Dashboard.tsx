import Map from './Map';
import Chart from './Chart';

function Dashboard() {
  return (
    <main>
      <Map />
      <Chart />
    </main>
  );
}

export default Dashboard;
