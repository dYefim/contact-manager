import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';
import { useChartsData } from '../../../api/api';
import { abbreviateNumber } from '../../../utils/abbrevianeNumUtil';

const Chart = () => {
  const { data, isLoading } = useChartsData();

  const getFormattedData = () => {
    return Object.entries(data.cases).map(([date, cases]) => ({
      date,
      cases,
    }));
  };

  if (isLoading) {
    return <h1>Loading Chart... </h1>;
  }

  return (
    <ResponsiveContainer width="96%" height="46%" minHeight="200px">
      <LineChart data={getFormattedData()}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="date" minTickGap={35} />
        <YAxis tickFormatter={abbreviateNumber} />
        <Tooltip formatter={abbreviateNumber} />
        <Legend />
        <Line type="monotone" dataKey="cases" stroke="#8884d8" />
      </LineChart>
    </ResponsiveContainer>
  );
};

export default Chart;
