import React from 'react';
import {
  createBrowserRouter,
  Navigate,
  RouterProvider,
} from 'react-router-dom';

import Navbar from './components/Navbar';
import ContactPage from './pages/Contacts';
import Dashboard from './pages/Dashboard/Dashboard';

import './App.css';
import CreateContactPage from './pages/CreateContact/CreateContactPage';
import EditContactPage from './pages/EditContact/EditContactPage';

const withNavbar = (component: React.ReactElement) => (
  <>
    <Navbar />
    {component}
  </>
);

const router = createBrowserRouter([
  {
    path: '/',
    element: <Navigate to="/contacts/" replace />,
  },
  {
    path: '/contacts/',
    element: withNavbar(<ContactPage />),
  },
  {
    path: 'contacts/create',
    element: withNavbar(<CreateContactPage />),
  },
  {
    path: 'contacts/edit',
    element: withNavbar(<EditContactPage />),
  },
  {
    path: '/dashboard',
    element: withNavbar(<Dashboard />),
  },
]);

function App() {
  return (
    <div className="App">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
